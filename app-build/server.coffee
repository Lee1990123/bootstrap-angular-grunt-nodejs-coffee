process.chdir __dirname

express = require "express"
bodyParser = require "body-parser"
jf = require "jsonfile"

app = express()
app.use bodyParser.urlencoded {extended: false}
app.use bodyParser.json()

# Web Server
app.use express.static "./public/"

# Start
jf.readFile "./package.json", (err, obj) ->
  npmPackage = obj
  port = npmPackage.ports.main
  app.listen port, ->
    console.log 'Express server listening on port %d in %s mode', port, app.settings.env