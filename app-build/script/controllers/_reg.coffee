define([
  './mainController'
], (
  mainController
) ->
  # Return components as Name/Object pairs
  {
  mainController: mainController
  }
)