define([
  '_'
],(
  _
) ->
  [
    '$scope'
    (
      $scope
    ) ->

      class SampleController
        myFunc: =>
          console.log "Hello World"

      # Expose
      $scope.sampleController = new SampleController()

  ]
)