# Shim
navigator.getUserMedia =
  navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia

# Work-around for Angular JQlite lack of selectors
window.query = (selector, dom = document) ->
  dom.querySelectorAll selector

define([
  "Angular"
  "AngularRegistration"
  "controllers/_reg"
  "directives/_reg"
  "filters/_reg"
  "services/_reg"
], (
  angular
  reg
  controllersMap
  directivesMap
  filtersMap
  servicesMap
) ->

  # Info
  APP_NAME = "MainApp"

  ###*
  # The initialization process
  #
  # @class MainApp
  # @constructor
  # @param {AngularModule} @app Angular Module to register directives, filters and services
  ###

  class MainApp
    name: APP_NAME
    ###*
    # Wire in Angular components and initializes the project
    #
    # @method initialize
    ###
    initialize: (deps = []) =>

      # Initialize Once
      return if @isInit
      @isInit = true

      # Create Main App
      @app = angular.module(APP_NAME, deps)
      console.log "#### APP:", APP_NAME, "initialize"
      reg.register @app, controllersMap, "controller"
      reg.register @app, directivesMap, "directive"
      reg.register @app, filtersMap, "filter"
      reg.register @app, servicesMap, "factory", false

      # Return
      @app
    ###*
    # Start up the application
    #
    # @method initialize
    ###
    startup: =>
      @app.run([
        '$http'
        (
          $http
        ) ->
          # AngularJS setting to work with CORS
          delete $http.defaults.headers.common['X-Requested-With']

      ])

  # Return
  new MainApp()

)