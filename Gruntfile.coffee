# Info
BUILD_SERVER = "./app-build/"
DEV_SERVER = "./app-dev/"
RELEASE_SERVER = "./app-release/"

module.exports = (grunt) ->

  npmPackage = grunt.file.readJSON "package.json"

  # Grunt Tasks
  gruntTaskList = {
    default: [
      "coffeelint"
      "clean:dev"
      "mkdir:dev"
      "coffee:devPublicScript"
      "sass:dev"
      "copy:dev"
      "symlink:components"
      "symlink:public"
      "coffee:devServer"
      "exec:rundev"
    ]
    devwatch: [
      "watch"
    ]
    clear: [
      "clean"
    ]
    release: [
      "coffeelint"
      "clean:release"
      "mkdir:release"
      "sass:release"
      "copy:release"
      "coffee:devPublicScript"
      "requirejs"
      "coffee:releaseServer"
      "clean:dev"
#      "exec:runserver"
    ]
  }

  # Configuration
  grunt.initConfig
    name: npmPackage.name

# Delete output directories
    clean:
      release: [
        RELEASE_SERVER
        BUILD_SERVER + "public/script"
        BUILD_SERVER + "public/style"
        BUILD_SERVER + "public/components"
      ]
      dev: [
        DEV_SERVER
        BUILD_SERVER + "public/script"
        BUILD_SERVER + "public/style"
        BUILD_SERVER + "public/components"
      ]

# Create directory structure
    mkdir:
      dev:
        options:
          mode: "0700"
          create: [
            DEV_SERVER
          ]
      release:
        options:
          mode: "0700"
          create: [
            RELEASE_SERVER
          ]

# Create reference shortcuts
    symlink:
      components:
        dest: BUILD_SERVER + "public/components"
        relativeSrc: "./../../bower_components/"
        options:
          type: "dir"
      public:
        dest: DEV_SERVER + "public"
        relativeSrc: "./../" + BUILD_SERVER + "public/"

# Copy HTML and resource dependancies
    copy:
      dev:
        files: [
          {
            src: "package.json"
            dest: DEV_SERVER + "package.json"
          }
        ]
      release:
        files: [
          {
            expand: true
            cwd: "bower_components"
            src: "**/*.js"
            dest: BUILD_SERVER + "public/components"
          }
          {
            expand: true
            cwd: "bower_components"
            src: "**/*.js"
            dest: RELEASE_SERVER + "public/components"
          }
          {
            expand: true
            cwd: BUILD_SERVER
            src: "public/**"
            dest: RELEASE_SERVER
          }
          {
            src: "package.json"
            dest: RELEASE_SERVER + "package.json"
          }
        ]

# Ensure we're using good coding standards
    coffeelint:
      build: [
        BUILD_SERVER + "/**/*.coffee"
      ]
      options:
        max_line_length:
          value: 120

# Convert CoffeeScript into JavaScript
    coffee:
      devServer:
        expand: true,
        cwd: BUILD_SERVER
        src: "*.coffee"
        dest: DEV_SERVER
        ext: ".js"
      releaseServer:
        expand: true,
        cwd: BUILD_SERVER
        src: "*.coffee"
        dest: RELEASE_SERVER
        ext: ".js"
      devPublicScript:
        expand: true,
        cwd: BUILD_SERVER + "script/"
        src: "**/*.coffee"
        dest: BUILD_SERVER + "public/script/"
        ext: ".js"

# Compile RequireJS main-app structure
    requirejs:
      build:
        options:
          name: npmPackage.angularAppName
          baseUrl: BUILD_SERVER + "public/script"
          mainConfigFile: BUILD_SERVER + "public/script/requirejs-config.js"
          exclude: [
            "Angular"
          ]
          include: [
            "requirejs-config.js"
          ]
          out: RELEASE_SERVER + "public/script/requirejs-config.js"

# Convert SASS into CSS
    sass:
      dev:
        files:
          "./app-build/public/style/main.css": "./app-build/style/main.sass"
        options:
          style: "expanded"
      release:
        files:
          "./app-build/public/style/main.css": "./app-build/style/main.sass"
        options:
          style: "compressed"

# Watch for changes in development
    watch:
      options:
        livereload: npmPackage.ports.livereload
      dev:
        files: [
          BUILD_SERVER + "public/index.html"
          BUILD_SERVER + "style/**/*.sass"
          BUILD_SERVER + "**/*.coffee"
        ]
        tasks: [
          "sass:dev"
          "coffeelint"
          "coffee:devPublicScript"
          "coffee:devServer"
        ]

# Run Web Server
    exec:
      runserver: "cd #{RELEASE_SERVER}; node " + npmPackage.main
      rundev: "cd #{DEV_SERVER}; node " + npmPackage.main


  # Load NPM modules
  matchdep = require "matchdep"
  matchdep.filterDev("grunt-*").forEach(grunt.loadNpmTasks)

  # Register Tasks
  for taskName, taskList of gruntTaskList
    grunt.registerTask taskName, taskList